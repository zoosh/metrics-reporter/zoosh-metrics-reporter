declare const listener: (port: number, metricsAttributes: string[]) => void;
export default listener;
