declare const reporter: (cronPattern: string, locale: string, endPoint: string, metricsAttributes: string[]) => Promise<void>;
export { reporter };
