"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const listener_1 = __importDefault(require("./listener"));
const reporter_1 = require("../reporter");
const MetricsReporterStandalone = { listener: listener_1.default, reporter: reporter_1.reporter };
exports.default = MetricsReporterStandalone;
//# sourceMappingURL=index.js.map