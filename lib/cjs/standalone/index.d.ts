declare const MetricsReporterStandalone: {
    listener: (port: number, metricsAttributes: string[]) => void;
    reporter: (cronPattern: string, locale: string, endPoint: string, metricsAttributes: string[]) => Promise<void>;
};
export default MetricsReporterStandalone;
