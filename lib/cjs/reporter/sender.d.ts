declare const sender: (endPoint: string, data: Record<string, unknown>) => Promise<void>;
export default sender;
