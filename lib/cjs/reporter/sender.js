"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const sender = async (endPoint, data) => {
    try {
        await axios_1.default.post(endPoint, data);
    }
    catch (err) {
        console.error(err);
    }
};
exports.default = sender;
//# sourceMappingURL=sender.js.map