"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reporter = void 0;
const cron_1 = require("cron");
const metrix_1 = __importDefault(require("../metrix"));
const sender_1 = __importDefault(require("./sender"));
const reporter = async (cronPattern, locale, endPoint, metricsAttributes) => {
    new cron_1.CronJob(cronPattern, async () => await (0, sender_1.default)(endPoint, (0, metrix_1.default)(metricsAttributes)), null, true, locale);
};
exports.reporter = reporter;
//# sourceMappingURL=reporter.js.map