"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const metrix_1 = __importDefault(require("../metrix"));
const listener = (req, res, metricsAttributes) => {
    res.json((0, metrix_1.default)(metricsAttributes));
};
exports.default = listener;
//# sourceMappingURL=listener.js.map