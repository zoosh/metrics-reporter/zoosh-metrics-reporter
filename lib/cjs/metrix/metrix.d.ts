declare const metrix: (attributes: string[]) => Record<string, string | number>;
export default metrix;
