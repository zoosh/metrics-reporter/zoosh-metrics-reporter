"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MetricsReporterStandalone = exports.MetricsReporterExpress = void 0;
const expressMiddleware_1 = __importDefault(require("./expressMiddleware"));
exports.MetricsReporterExpress = expressMiddleware_1.default;
const standalone_1 = __importDefault(require("./standalone"));
exports.MetricsReporterStandalone = standalone_1.default;
//# sourceMappingURL=index.js.map