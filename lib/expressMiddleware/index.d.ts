/// <reference types="qs" />
/// <reference types="express" />
declare const MetricsReporterExpress: {
    listener: (req: import("express").Request<import("express-serve-static-core").ParamsDictionary, any, any, import("qs").ParsedQs, Record<string, any>>, res: import("express").Response<any, Record<string, any>>, metricsAttributes: string[]) => void;
    reporter: (cronPattern: string, locale: string, endPoint: string, metricsAttributes: string[]) => Promise<void>;
};
export default MetricsReporterExpress;
