import { Request, Response } from 'express';
declare const listener: (req: Request, res: Response, metricsAttributes: string[]) => void;
export default listener;
