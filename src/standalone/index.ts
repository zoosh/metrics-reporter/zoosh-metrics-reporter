import listener from './listener';
import { reporter } from '../reporter';

const MetricsReporterStandalone = { listener, reporter };

export default MetricsReporterStandalone;
