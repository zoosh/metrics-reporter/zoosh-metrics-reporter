import * as http from 'http';
import metrix from '../metrix';

const listener = (port: number, metricsAttributes: string[]): void => {
  const server = http.createServer((req, res): void => {
    res.setHeader('Content-Type', 'application/json');
    res.writeHead(200);
    res.end(JSON.stringify(metrix(metricsAttributes)));
  });
  server.listen(port);
};

export default listener;
