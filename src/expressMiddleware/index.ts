import listener from './listener';
import { reporter } from '../reporter';

const MetricsReporterExpress = { listener, reporter };

export default MetricsReporterExpress;
