import { Request, Response } from 'express';
import metrix from '../metrix';

const listener = (req: Request, res: Response, metricsAttributes: string[]): void => {
  res.json(metrix(metricsAttributes));
};

export default listener;
