import { CronJob } from 'cron';
import metrix from '../metrix';
import sender from './sender';

const reporter = async (
  cronPattern: string,
  locale: string,
  endPoint: string,
  metricsAttributes: string[],
): Promise<void> => {
  new CronJob(
    cronPattern,
    async () => await sender(endPoint, metrix(metricsAttributes)),
    null,
    true,
    locale,
  );
};

export { reporter };
