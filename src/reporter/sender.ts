import axios from 'axios';

const sender = async (endPoint: string, data: Record<string, unknown>): Promise<void> => {
  try {
    await axios.post(endPoint, data);
  } catch (err) {
    console.error(err);
  }
};

export default sender;
